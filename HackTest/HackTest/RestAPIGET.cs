﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HackTest
{
    class RestAPIGET
    {

        public List<T> Get<T>(string APIurl)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(APIurl);
            request.Method = "GET";
            HttpWebResponse respose = (HttpWebResponse)request.GetResponse();
            var read = respose.GetResponseStream();
            StreamReader reader = new StreamReader(read);
            var Final = reader.ReadToEnd();
            var Finalresult = JsonConvert.DeserializeObject<T>(Final);
            return Finalresult;


        }
            

    }