﻿Feature: GetLoanApplication
	In order to get get loan application
	As a API consumer
	I want to retrieve the loan applicant details

@SanityTest, Regression Test
Scenario: Get Loan Details - Happy Path
	Given I request for searching loan application
	And I want request header details 
	| Name         | Value            |
	| accept | application/json |
	When I request for response
	Then response status code should be '200'
	And response header should valid response
		| Name | Value |
		| Server    |  null     |
	And Response should contain response message body
	And Responce body should return loan application data	
	And Response body should return loan data
	| Name   | Value   |
	| loadId	| 1000007 |
	| First Name       | Prabhu   |
	| creatorId      | 0 |
	| approverId  | 0  |

	
@NegativeTest
Scenario: Get Loan Details - Invalid loan applicant ID
	Given I request for searching loan application
	And I want request header details 
	| Name         | Value            |
	| Content-Type | application/Json |
	When I request for response
	Then response status code should be '404'
	And response header should valid response
		| Name | Value |
		| Cache Control     |  max-age=0     |
	And Response should not contain response message body
	And Responce body should return loan application data
	

