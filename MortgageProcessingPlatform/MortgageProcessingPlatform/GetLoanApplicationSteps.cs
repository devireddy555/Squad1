﻿using System;
using TechTalk.SpecFlow;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Collections.Generic;
using System.Linq;

namespace MortgageProcessingPlatform
{
    [Binding]
    public class GetLoanApplicationSteps : API
    {
        [Given(@"I request for searching loan application")]
        public void GivenIRequestForSearchingLoanApplication()
        {
            RequestURL = @"http://lp-gbcrvy1:8080/SquadAlpha/getQueueList";
        }
        
        [Given(@"I want request header details")]
        public void GivenIWantRequestHeaderDetails(Table table)
        {
            RequestHeaders = new Dictionary<string,string>();
            foreach (var header in table.Rows)
            {
                RequestHeaders.Add(header["Name"], header["Value"]);
            }
        }
        
        [When(@"I request for response")]
        public void WhenIRequestForResponse()
        {
            GetResponse();
        }
        
        [Then(@"response status code should be '(.*)'")]
        public void ThenResponseStatusCodeShouldBe(int expectedStatusCode)
        {
            Assert.AreEqual(expectedStatusCode, (int)ResponseMessage.StatusCode);
        }
        
        [Then(@"response header should valid response")]
        public void ThenResponseHeaderShouldValidResponse(Table table)
        {
            //foreach (var row in table.Rows)
            //{
            //    var expectedName = row["Name"];
            //    var expectedValue = row["Value"];
            //    var i = ResponseMessage.Headers.GetValues(expectedName).First();
            //    Assert.AreEqual(expectedValue, ResponseMessage.Headers.GetValues(expectedName).First());
            //}
        }
        
        [Then(@"Response should contain response message body")]
        public void ThenResponseShouldContainResponseMessageBody()
        {
           Assert.IsFalse(string.IsNullOrEmpty(ResponseBody));
        }
        
        [Then(@"Responce body should return loan application data")]
        public void ThenResponceBodyShouldReturnLoanApplicationData(Table table)
        {
           
        }
        
        [Then(@"Response should not contain response message body")]
        public void ThenResponseShouldNotContainResponseMessageBody()
        {
            Assert.IsTrue(string.IsNullOrEmpty(ResponseBody));
        }
        
       
        [Then(@"Response body should return loan data")]
        public void ThenResponseBodyShouldReturnLoanData(Table table)
        {
            foreach (var row in table.Rows)
            {
                var dataHeader = row["Name"];
                var dataValue = row["Value"];
                var i = Response.Value<String>(dataHeader);
                Assert.AreEqual(dataValue, Response.Value<string>(dataHeader));
            }
        }

    }
}
