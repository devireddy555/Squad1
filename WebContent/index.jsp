<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SquadAlpha</title>
</head>
<body>
	<h2>Hello SquadAlpha</h2>
	<BR>
	<BR>
	<h4>POST - > LP-GBCRVY1:8080//SquadAlpha/saveApplication</h4>
	<p>
		 {
            "creatorId": 2,
            "firstName": "A",
            "lastName": "B",
            "address": "C",
            "creditStatus": 1,
            "applicantIncome": 8,
            "loanAmount": 9
		}
	</p>
	<BR>
	<h4>GET - > LP-GBCRVY1:8080//SquadAlpha/search/{loanId}</h4>
	<BR>
	<h4>PUT  - > LP-GBCRVY1:8080//SquadAlpha/approve</h4>
	<p>
		 {	
 			"loanId": "1000002",
 			"approverId": "2",
 			"creditStatus" : "2"
		}
	</p>
	<BR>
	<h4>PUT  - > LP-GBCRVY1:8080//SquadAlpha/lendAmount</h4>
	<p>
		 {	
 			"loanId": "1000002",
 			"lenderId": "3",
 			"creditStatus" : "3"
		}
		
	</p>
	<BR>
	<h4>GET  - > http://lp-gbcrvy1:8080/SquadAlpha/getQueueList</h4>
	<p></p>
</body>
</html>