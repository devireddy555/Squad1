package com.Dao;

import java.util.List;

import com.vo.Application;

public interface LoanApplicationDao {
	
	public int saveApplication(Application application, List<String> errors);
	public Application getLoanDetails(int loadId, List<String> errors);
}
