package com.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.constants.DatabaseConnection;
import com.vo.Application;

public class LoanApplicationDaoImpl implements LoanApplicationDao {
	private Connection connection = null;
	private PreparedStatement preparedStatement = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	
	/**
	 * closeConnection
	 * This method will be used to close the all connection which are opened for DB 
	 * @param errors
	 */
	private void closeConnection(List<String> errors){
		try {
			if(null != resultSet){
				resultSet.close();
			}
		} catch (SQLException e) {
			errors.add("ResultSet Issue! Contact Admin");
		}
		try {
			if(null != null){
				preparedStatement.close();
			}
		} catch (SQLException e) {
			errors.add("PreparedStatement Issue! Contact Admin");
		}
		try {
			if(null != statement){
				statement.close();
			}
		} catch (SQLException e) {
			errors.add("Statement Issue! Contact Admin");
		}
		try {
			if(null != connection){
				connection.close();
			}
		} catch (SQLException e) {
			errors.add("Connection Issue! Contact Admin");
		}
	}
	
	
	/*
	 * This method is used to generate the Primary Key Loan Id
	 * @param errors
	 * */
	private int getPrimaryKey(List<String> errors){
		int loanId = 0;
		connection = DatabaseConnection.getConnection();
		String sql = "SELECT MAX(loan_id) as loan_id FROM applications_master";
		try {
			preparedStatement = connection.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				loanId = resultSet.getInt("loan_id");
				if(0 == loanId){
					loanId = 1000000;
				}
				loanId++;
			}
		}catch (SQLException e) {
			errors.add("SQLException");
		}finally{
			closeConnection(errors);
		}
		return loanId;
	}

	
	/**
	 * this method will be used add the application and data will be saved in the DB
	 * @param quote
	 * @param errors
	 * @return saved/not flag
	 */
	@Override
	public int saveApplication(Application application, List<String> errors) {
		int flag = 0;
		int loanId = getPrimaryKey(errors);
		application.setLoanId(loanId);
		connection = DatabaseConnection.getConnection();
		String sql = "INSERT INTO applications_master(loan_id, creator_id, approver_id ,lender_id ,"
				+ "first_name ,last_name ,address ,credit_status ,"
				+ "applicant_income ,loan_amount ,time_created) VALUES (?,?,?,?, ?,?,?,?, ?,?,Current_TimeStamp)";
		try {
			
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, application.getLoanId());
			preparedStatement.setInt(2, application.getCreatorId());
			preparedStatement.setInt(3, application.getApproverId());
			preparedStatement.setInt(4, application.getLenderId());
			
			preparedStatement.setString(5,application.getFirstName());
			preparedStatement.setString(6,application.getLastName());
			preparedStatement.setString(7, application.getAddress());
			preparedStatement.setInt(8, application.getCreditStatus());
			
			preparedStatement.setInt(9, application.getApplicantIncome());
			preparedStatement.setInt(10,application.getLoanAmount());
			
			flag = preparedStatement.executeUpdate();
			
			if(0!= flag){
				flag = application.getLoanId();
			}
		} catch (SQLException e) {
			errors.add("SQLException");
		}finally{
			closeConnection(errors);
		}
		return flag;
	}
	
	/**
	 * this method will be used get the application object from the DB
	 * @param loadId
	 * @param errors
	 * @return application
	 */
	public Application getLoanDetails(int loadId, List<String> errors){
		Application application = null;
		connection = DatabaseConnection.getConnection();
		String sql = "SELECT * FROM applications_master where loan_id = ?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, loadId);
			
			resultSet = preparedStatement.executeQuery();
			if(resultSet.next()){
				application = new Application();
				application.setLoanId(resultSet.getInt("loan_id"));
				application.setCreatorId(resultSet.getInt("creator_id"));
				application.setApproverId(resultSet.getInt("approver_id"));
				application.setLenderId(resultSet.getInt("lender_id"));

				
				application.setFirstName(resultSet.getString("first_name"));
				application.setLastName(resultSet.getString("last_name"));
				application.setAddress(resultSet.getString("address"));
				application.setCreditStatus(resultSet.getInt("credit_status"));

				application.setApplicantIncome(resultSet.getInt("applicant_income"));
				application.setLoanAmount(resultSet.getInt("loan_amount"));
				application.setTimeCreated(resultSet.getString("time_created"));
				application.setModified(resultSet.getString("modified"));
			}
		}catch (SQLException e) {
			errors.add("SQLException");
		}finally{
			closeConnection(errors);
		}
		return application;
	}
	
	public static void main(String[] args) {
		LoanApplicationDaoImpl obj = new LoanApplicationDaoImpl();
		System.out.println(obj.getPrimaryKey(new ArrayList<String>()));
	}
}
