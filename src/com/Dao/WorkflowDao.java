package com.Dao;

import java.util.List;

import com.vo.Application;

public interface WorkflowDao {

	public int approve(int loanId, int creditStatus, int approver, List<String> errors);

	public int lendAmount(int loanId, int creditStatus, int lender, List<String> errors);
	
	public List<Application> getQueueList(List<String> errors);
}
