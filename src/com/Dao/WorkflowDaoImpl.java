package com.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.constants.DatabaseConnection;
import com.vo.Application;

public class WorkflowDaoImpl implements WorkflowDao{

	private Connection connection = null;
	private PreparedStatement preparedStatement = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	
	/**
	 * closeConnection
	 * This method will be used to close the all connection which are opened for DB 
	 * @param errors 
	 */
	private void closeConnection(List<String> errors){
		try {
			if(null != resultSet){
				resultSet.close();
			}
		} catch (SQLException e) {
			errors.add("ResultSet Issue! Contact Admin");
		}
		try {
			if(null != null){
				preparedStatement.close();
			}
		} catch (SQLException e) {
			errors.add("PreparedStatement Issue! Contact Admin");
		}
		try {
			if(null != statement){
				statement.close();
			}
		} catch (SQLException e) {
			errors.add("Statement Issue! Contact Admin");
		}
		try {
			if(null != connection){
				connection.close();
			}
		} catch (SQLException e) {
			errors.add("Connection Issue! Contact Admin");
		}
	}
	

	/*
	 * This method will be used to approve the loan
	 * @param loanId,
	 * @param creditStatus
	 * @param approver
	 * @param errors
	 * */
	@Override
	public int approve(int loanId, int creditStatus, int approver, List<String> errors){
		int loanIdFlag = 0;
		connection = DatabaseConnection.getConnection();
		String sql = "UPDATE applications_master SET credit_status = ?, approver_id = ? WHERE loan_id = ?";
		try{
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, creditStatus);
			preparedStatement.setInt(2, approver);
			preparedStatement.setInt(3, loanId);
			loanIdFlag = preparedStatement.executeUpdate();
			if(0 != loanIdFlag){
				loanIdFlag = loanId;
			}
		}catch (Exception e) {
			errors.add("SQL Exception - Contact Admin");
		}finally {
			closeConnection(errors);
		}
		
		return loanIdFlag;
	}

	/*
	 * This method will be used to lend the loan
	 * @param loanId,
	 * @param creditStatus
	 * @param lender
	 * @param errors
	 * */
	@Override
	public int lendAmount(int loanId, int creditStatus, int lender, List<String> errors){
		int loanIdFlag = 0;
		connection = DatabaseConnection.getConnection();
		String sql = "UPDATE applications_master SET credit_status = ?, lender_id = ? WHERE loan_id = ?";
		try{
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, creditStatus);
			preparedStatement.setInt(2, lender);
			preparedStatement.setInt(3, loanId);
			loanIdFlag = preparedStatement.executeUpdate();
			if(0 != loanIdFlag){
				loanIdFlag = loanId;
			}
		}catch (Exception e) {
			errors.add("SQL Exception - Contact Admin");
		}finally {
			closeConnection(errors);
		}
		return loanIdFlag;
	}
	
	/*
	 * This method will be used to show the work list
	 * @param errors
	 * */
	@Override
	public List<Application> getQueueList(List<String> errors){
		connection = DatabaseConnection.getConnection();
		List<Application> applications = new ArrayList<Application>();
		String sql = "SELECT * FROM applications_master ORDER BY credit_status";
		try{
			preparedStatement = connection.prepareStatement(sql);
			resultSet = preparedStatement.executeQuery();

			while(resultSet.next()){
				Application application = new Application();
				application.setLoanId(resultSet.getInt("loan_id"));
				application.setCreatorId(resultSet.getInt("creator_id"));
				application.setApproverId(resultSet.getInt("approver_id"));
				application.setLenderId(resultSet.getInt("lender_id"));

				
				application.setFirstName(resultSet.getString("first_name"));
				application.setLastName(resultSet.getString("last_name"));
				application.setAddress(resultSet.getString("address"));
				application.setCreditStatus(resultSet.getInt("credit_status"));

				application.setApplicantIncome(resultSet.getInt("applicant_income"));
				application.setLoanAmount(resultSet.getInt("loan_amount"));
				application.setTimeCreated(resultSet.getString("time_created"));
				application.setModified(resultSet.getString("modified"));

				applications.add(application);
			}
		}catch (Exception e) {
			errors.add("SQL Exception - Contact Admin");
		}finally {
			closeConnection(errors);
		}
		return applications;
	}
}
