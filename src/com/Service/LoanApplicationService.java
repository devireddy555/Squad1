package com.Service;

import org.springframework.web.bind.annotation.PathVariable;

import com.vo.Application;
import com.vo.ApplicationResponse;

public interface LoanApplicationService {

	public ApplicationResponse saveApplication(Application application);
	public ApplicationResponse search(@PathVariable Integer loanId);
}