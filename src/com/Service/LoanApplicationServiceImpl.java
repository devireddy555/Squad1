package com.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import com.Dao.LoanApplicationDao;
import com.Dao.LoanApplicationDaoImpl;
import com.vo.Application;
import com.vo.ApplicationResponse;

public class LoanApplicationServiceImpl implements LoanApplicationService{
	@Autowired
	LoanApplicationDao applicationDao;
	
	@Override
	public ApplicationResponse saveApplication(Application application) {	
		ApplicationResponse applicationResponse = new ApplicationResponse();
		List<String> errors = new ArrayList<String>();
		if(isValid(application)){
			int loadId = applicationDao.saveApplication(application, errors);
			application = applicationDao.getLoanDetails(loadId, errors);
		}else{
			errors.add("Kindly provide the valid Details");
		}
		
		List<Application> applications = new ArrayList<Application>();
		applications.add(application);
		applicationResponse.setApplications(applications);
		applicationResponse.setErrors(errors);
		return applicationResponse;
	}
	
	private boolean isValid(Application application){
		if(null == application.getFirstName() ||
				null == application.getLastName() ||
				null == application.getAddress() ||
				0 >= application.getLoanAmount() ||
				0 >= application.getApplicantIncome()
				){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public ApplicationResponse search(Integer loanId) {
		ApplicationResponse applicationResponse = new ApplicationResponse();
		List<String> errors = new ArrayList<String>();
		Application application = applicationDao.getLoanDetails(loanId, errors);
		
		List<Application> applications = new ArrayList<Application>();
		applications.add(application);
		applicationResponse.setApplications(applications);
		applicationResponse.setErrors(errors);
		return applicationResponse;
	}
}
