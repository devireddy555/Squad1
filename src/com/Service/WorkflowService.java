package com.Service;

import com.vo.Application;
import com.vo.ApplicationResponse;

public interface WorkflowService {
	
	public ApplicationResponse approve(Application application);
	
	public ApplicationResponse lendAmount(Application application);

	public ApplicationResponse getQueueList();
}

