package com.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.Dao.LoanApplicationDao;
import com.Dao.WorkflowDao;
import com.vo.Application;
import com.vo.ApplicationResponse;

public class WorkflowServiceImpl implements WorkflowService {

	
	@Autowired
	WorkflowDao workflowDao;
	
	@Autowired 
	LoanApplicationDao applicationDao;
	
	public ApplicationResponse approve(Application application){
		ApplicationResponse applicationResponse = new ApplicationResponse();
		List<String> errors = new ArrayList<String>();
		int loanId = 0;
		if((2 == application.getApproverId() || 3 == application.getApproverId()) &&
				2 == application.getCreditStatus()){
			loanId = workflowDao.approve(application.getLoanId(), application.getCreditStatus(), application.getApproverId(), errors);
		}else{
			errors.add("You are not authorised to approve.");
		}
		if(0 != loanId){
			application = applicationDao.getLoanDetails(application.getLoanId(), errors);
		}else{
			errors.add("Approval Failed, contact Administrator");
		}
		List<Application> applications = new ArrayList<Application>();
		applications.add(application);
		
		applicationResponse.setErrors(errors);
		applicationResponse.setApplications(applications);
		
		return applicationResponse;
	}

	public ApplicationResponse lendAmount(Application application){
		ApplicationResponse applicationResponse = new ApplicationResponse();
		List<String> errors = new ArrayList<String>();
		List<Application> applications = new ArrayList<Application>();
		int loanId = 0 ;
		if(3 == application.getLenderId() && 3 == application.getCreditStatus()){
			loanId = workflowDao.lendAmount(application.getLoanId(), application.getCreditStatus(), application.getLenderId(), errors);
		}else{
			errors.add("You are not authorised to lend");
		}
		if(0 != loanId){
			application = applicationDao.getLoanDetails(application.getLoanId(), errors);
		}else{
			errors.add("Lending Failed, contact Administrator");
		}
		applications.add(application);
		
		applicationResponse.setErrors(errors);
		applicationResponse.setApplications(applications);
		
		
		return applicationResponse;
	}

	@Override
	public ApplicationResponse getQueueList(){
		ApplicationResponse applicationResponse = new ApplicationResponse();
		List<String> errors = new ArrayList<String>();
		List<Application> applications = workflowDao.getQueueList(errors);
		
		applicationResponse.setErrors(errors);
		applicationResponse.setApplications(applications);
		return applicationResponse;
	}


}
