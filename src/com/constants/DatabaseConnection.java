package com.constants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DatabaseConnection {

	/**
	 * This will give the Connection object to connect to DB 
	 * @return Connection
	 */
	public static Connection getConnection() {
		Connection connection = null;		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = (Connection) DriverManager.getConnection("jdbc:mysql://hackathon2018.cp19kzpsbwaf.us-west-2.rds.amazonaws.com:3306/squadAlpha", "HackathonApr2018", "Tower42017");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		return connection;
	}
	
	public static void main(String[] args) {
		System.out.println(getConnection());
	}
	
/*	
	CREATE TABLE applications_master(
			loan_id int(9) PRIMARY KEY, 
			creator_id int(9) NOT NULL,
			approver_id int(9) NOT NULL,
			lender_id int(9) NOT NULL,
			first_name varchar(255) NOT NULL,
			last_name varchar(255) NOT NULL,
			address varchar(500) NOT NULL,
			credit_status int(9) NOT NULL,
			applicant_income int(9) NOT NULL,
			loan_amount int(9) NOT NULL,
			modified TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			time_created TIMESTAMP NOT NULL
		);
	*/
}
