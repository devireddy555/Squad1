package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Service.LoanApplicationService;
import com.Service.WorkflowService;
import com.vo.Application;
import com.vo.ApplicationResponse;

@Controller("/SquadAlpha")
public class MortgageController {

	@Autowired
	LoanApplicationService loanApplicationService;

	@Autowired 
	WorkflowService workflowService;
	
	@ResponseBody
	@RequestMapping(value = "/saveApplication", consumes=MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
	public ApplicationResponse saveApplication(@RequestBody Application application) {
		return loanApplicationService.saveApplication(application);
	}
	
	@ResponseBody
	@RequestMapping(value="/approve", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ApplicationResponse approve(@RequestBody Application application){
		return workflowService.approve(application);
	}

	@RequestMapping(value="/lendAmount", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ApplicationResponse lendAmount(@RequestBody Application application){
		return workflowService.lendAmount(application);
	}

	@ResponseBody
	@RequestMapping(value="/getQueueList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ApplicationResponse getQueueList(){
		return workflowService.getQueueList();
	}

	@ResponseBody
	@RequestMapping(value="/search/{loanId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ApplicationResponse search(@PathVariable Integer loanId){
		return loanApplicationService.search(loanId);
	}

}
