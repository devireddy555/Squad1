package com.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.Service.LoanApplicationServiceImpl;
import com.vo.ApplicationResponse;

@RunWith(MockitoJUnitRunner.class)
public class LoanApplicationServiceImplTest {

	@Mock
	LoanApplicationServiceImpl applicationServiceImplTest = new LoanApplicationServiceImpl();

	@InjectMocks
	ApplicationResponse applicationResponseMock = new ApplicationResponse();
	
	@Test
	public void search() {
		int loanId = 0;
		ApplicationResponse applicationResponse = applicationServiceImplTest.search(loanId);
		Assert.assertNull(applicationResponse);
	}
}
