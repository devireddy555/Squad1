package com.vo;

import java.io.Serializable;

public class Application implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9188255833783717178L;
	
	private int loanId;
	private int creatorId;
	private int approverId;
	private int lenderId;
	private String firstName;
	private String lastName;
	private String address;
	private int creditStatus;
	private int applicantIncome;
	private int loanAmount;
	private String timeCreated;
	private String modified;
	
	
	public int getLoanId() {
		return loanId;
	}
	public void setLoanId(int loanId) {
		this.loanId = loanId;
	}
	public int getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(int creatorId) {
		this.creatorId = creatorId;
	}
	public int getApproverId() {
		return approverId;
	}
	public void setApproverId(int approverId) {
		this.approverId = approverId;
	}
	public int getLenderId() {
		return lenderId;
	}
	public void setLenderId(int lenderId) {
		this.lenderId = lenderId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getCreditStatus() {
		return creditStatus;
	}
	public void setCreditStatus(int creditStatus) {
		this.creditStatus = creditStatus;
	}
	public int getApplicantIncome() {
		return applicantIncome;
	}
	public void setApplicantIncome(int applicantIncome) {
		this.applicantIncome = applicantIncome;
	}
	public int getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(int loanAmount) {
		this.loanAmount = loanAmount;
	}
	public String getTimeCreated() {
		return timeCreated;
	}
	public void setTimeCreated(String timeCreated) {
		this.timeCreated = timeCreated;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
}
