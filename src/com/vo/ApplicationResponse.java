package com.vo;

import java.util.List;

public class ApplicationResponse{
	private List<Application> applications;
	private List<String> errors;
	
	public List<Application> getApplications() {
		return applications;
	}
	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}
